package com.example.SpringSecurity.config;

import com.example.SpringSecurity.Entity.Role;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;



@Configuration
@EnableWebSecurity
public class BasicAuthSecurityConfiguration {
    @Bean
    SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http.authorizeHttpRequests(auth -> {
            auth
                    .requestMatchers("/api/getAllStudents").hasAnyAuthority("ROLE_USER","ROLE_ADMIN")
                    .requestMatchers("/api/addStudent").hasRole("ADMIN")
                    .requestMatchers("/api/updateStudent/{id}").hasRole("ADMIN")
                    .requestMatchers("/api/deleteStudent/{id}").hasRole("ADMIN")
                    .anyRequest().authenticated();
        });
        http.sessionManagement(
                session ->
                        session.sessionCreationPolicy(
                                SessionCreationPolicy.STATELESS)
        );
        http.httpBasic(Customizer.withDefaults());
        http.csrf(csrf -> csrf.disable());
//        http.headers().frameOptions().sameOrigin(); #for h2database lang, para mabasa frames
        return http.build();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public UserDetailsService userDetailsService() {

        var admin = User.withUsername("admin")
                .password(passwordEncoder().encode("admin"))
                .roles(Role.ADMIN.name())
                .build();

        var user = User.withUsername("user")
                .password(passwordEncoder().encode("user"))
                .roles(Role.USER.name())
                .build();

        return new InMemoryUserDetailsManager(admin, user);
    }


}
