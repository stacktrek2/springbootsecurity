package com.example.SpringSecurity.Controller;

import com.example.SpringSecurity.Entity.Student;
import com.example.SpringSecurity.Service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class StudentController {
    @Autowired
    private StudentService studentService;

    @GetMapping("/getAllStudents")
    @Secured({"USER", "ADMIN"})
    public List<Student> getAllStudents(){
        return studentService.getAllStudents();
    }
    @PostMapping("/addStudent")
    @PreAuthorize("hasRole('ADMIN')")
    public Student addStudent(@RequestBody Student student){
        return studentService.addStudent(student);
    }

    @PutMapping("/updateStudent/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public Student updateStudent(@RequestBody Student student, @PathVariable int id){
        return studentService.updateStudent(student, id);
    }
    @DeleteMapping("/deleteStudent/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public String deleteStudent(@PathVariable int id){
        return studentService.deleteStudent(id);
    }

}
