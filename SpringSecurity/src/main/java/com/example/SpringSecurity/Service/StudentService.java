package com.example.SpringSecurity.Service;

import com.example.SpringSecurity.Entity.Student;
import com.example.SpringSecurity.Repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;


import java.util.Collections;
import java.util.List;

@Service
public class StudentService {
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

//    Getting the list of students
    public List<Student> getAllStudents(){
        return studentRepository.findAll();
    }

//    Adding a student
    public Student addStudent(Student student){
        student.setFirstName(student.getFirstName());
        student.setPassword(passwordEncoder.encode(student.getPassword()));

        return studentRepository.save(student);

    }
//  Updating a student
    public Student updateStudent(Student student, int id){
        Student existingStudent = studentRepository.findById(id).orElse(null);
        existingStudent.setFirstName(student.getFirstName());
        existingStudent.setLastName(student.getLastName());
        existingStudent.setEmail(student.getEmail());
        existingStudent.setCourse(student.getCourse());
        existingStudent.setGPA(student.getGPA());
        return studentRepository.save(existingStudent);
    }
//    Deleting a student
    public String deleteStudent(int id){
        studentRepository.deleteById(id);
        return "Student Removed " + id;
    }

}
